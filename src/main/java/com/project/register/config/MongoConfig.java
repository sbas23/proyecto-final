package com.project.register.config;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;

import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;

@Configuration
public class MongoConfig {
	
	@Value("${spring.data.mongodb.host}")
	private String host;
	@Value("${spring.data.mongodb.port}")
	private String port;
	@Value("${spring.data.mongodb.username}")
	private String username;
	@Value("${spring.data.mongodb.password}")
	private String password;
	@Value("${spring.data.mongodb.database}")
	private String database;

	@Bean
	public MongoDbFactory mongoDbFactory() throws UnknownHostException{
		MongoClient mongo;
		
		if(username == null || username.trim().length()==0){
			mongo = new MongoClient();
		}else{
			List<ServerAddress> seeds = new ArrayList<>();
			seeds.add( new ServerAddress(host,Integer.parseInt(port)));
			List<MongoCredential> credentials = new ArrayList<>();
			credentials.add(
					MongoCredential.createScramSha1Credential(username, database, password.toCharArray()));
			mongo = new MongoClient(seeds, credentials );
		}
		
		return new SimpleMongoDbFactory(mongo, database);
	}
		
	@Bean
	public MongoOperations mongoOperations() throws UnknownHostException{
		return new MongoTemplate(mongoDbFactory());
	}
}
