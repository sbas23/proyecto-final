package com.project.register.mongo.persistences;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.project.register.mongo.entities.Events;

public interface IEvents extends MongoRepository<Events, String>, IEventsCustom{
	
}
