package com.project.register.mongo.persistences;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.project.register.models.RegisterReportRequest;
import com.project.register.mongo.entities.Events;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class IEventsImpl implements IEventsCustom{

	@Autowired
	MongoTemplate mongoTemplate;
	
	@Override
	public List<Events> getDetailtReportByFilters(RegisterReportRequest auditReportRequest, boolean activatePagination) {
		
		try {
			Query query = new Query();
			List<Criteria> criteria = new ArrayList<>();
			
			/** FILTRO POR FECHAS PARAMETRO OBLIGATORIO*/
			criteria.add(Criteria.where("date").gte(auditReportRequest.getDateFrom()));
			criteria.add(Criteria.where("date").lte(auditReportRequest.getDateTo()));
			
			/** FILTRO USUARIOS MODIFICADOS PARAMETRO OBLIGATORIO*/
			if (auditReportRequest.getModifiedUsernameList() != null && !auditReportRequest.getModifiedUsernameList().isEmpty()) {
				criteria.add(Criteria.where("modifiedUser").in(auditReportRequest.getModifiedUsernameList()));
			} 
			
			/** FILTRO EVENTOS*/
			if (auditReportRequest.getEventList() != null && !auditReportRequest.getEventList().isEmpty()) {
				criteria.add(Criteria.where("event").in(auditReportRequest.getEventList()));
			}
				
			query.addCriteria(new Criteria().andOperator(criteria.toArray(new Criteria[criteria.size()])));
			query.with(new Sort(Sort.Direction.DESC,"date"));		
			log.info("Query: {}", query);
			return mongoTemplate.find(query, Events.class);
			
		} catch (Exception e) {
			log.error("Exception: {}", e);
			return new ArrayList<>();
		}
		
	}
	
	@Override
	public long countDetailtReportByFilters(RegisterReportRequest auditReportRequest) {
		
		try {
			Query query = new Query();
			List<Criteria> criteria = new ArrayList<>();
			
			/** FILTRO POR FECHAS PARAMETRO OBLIGATORIO*/
			criteria.add(Criteria.where("date").gte(auditReportRequest.getDateFrom()));
			criteria.add(Criteria.where("date").lte(auditReportRequest.getDateTo()));
			
			/** FILTRO USUARIOS MODIFICADOS PARAMETRO OBLIGATORIO*/
			if (auditReportRequest.getModifiedUsernameList() != null && !auditReportRequest.getModifiedUsernameList().isEmpty()) {
				criteria.add(Criteria.where("modifiedUser").in(auditReportRequest.getModifiedUsernameList()));
			} 
			
			/** FILTRO EVENTOS*/
			if (auditReportRequest.getEventList() != null && !auditReportRequest.getEventList().isEmpty()) {
				criteria.add(Criteria.where("event").in(auditReportRequest.getEventList()));
			}
				
			query.addCriteria(new Criteria().andOperator(criteria.toArray(new Criteria[criteria.size()])));
			log.debug("Query: {}", query);
			return mongoTemplate.count(query, Events.class);
			
		} catch (Exception e) {
			log.error("Exception: {}", e);
			return 0L;
		}
		
	}

}
