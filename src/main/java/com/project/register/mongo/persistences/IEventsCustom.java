package com.project.register.mongo.persistences;

import java.util.List;

import com.project.register.models.RegisterReportRequest;
import com.project.register.mongo.entities.Events;

public interface IEventsCustom {
	List<Events> getDetailtReportByFilters(RegisterReportRequest auditReportRequest, boolean activatePagination);
	
	long countDetailtReportByFilters(RegisterReportRequest auditReportRequest);
}
