package com.project.register.mongo.entities;

import java.util.Date;
import java.util.Map;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
@Document(collection = "events")
public class Events {
	@Id
	public String id;
	@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssZ", timezone = "America/Bogota") 	
	private Date date; 	
	private String requestingProduct;
	private String requestingUser;
	private String event;
	private String modifiedUser;
	private int modifiedClientId;
	private Map<String, Object> description;
}
