package com.project.register.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.register.models.AuditRequest;
import com.project.register.models.EventsRequest;
import com.project.register.models.RegisterReportRequest;
import com.project.register.mysql.entities.AuditEvents;
import com.project.register.mysql.persistences.IAuditEvents;
import com.project.register.services.RegisterReportService;
import com.project.register.services.RegisterService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping(value = "/register")
public class RegisterController {
	
	private static final String RECEIVED = "RECEIVED {}";	
	
	@Autowired
	RegisterService registerService;
	@Autowired
	RegisterReportService registerReportService;
	@Autowired
	IAuditEvents iAuditEvents;
	
	@PostMapping(value = "/trace", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<Object> getNotificationsListByFilters(@RequestBody AuditRequest requestAudit) {
		log.info(RECEIVED, requestAudit);
		return registerService.responseRegisterRequest(requestAudit);
	}
	
	@PostMapping(value = "/detail", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<Object> getDetail(@RequestBody RegisterReportRequest auditReportRequest) {
		log.info("Register request: {}", auditReportRequest);
		if (auditReportRequest != null && auditReportRequest.getDateFrom() != null && auditReportRequest.getDateTo() != null) {
			return registerReportService.getDetailByFilters(auditReportRequest);
		}
		log.warn("Error: Null consultation dates");
		Map<String, Object> data = new HashMap<>();
		data.put("Error", "Null consultation dates");
		return new ResponseEntity<>(data, HttpStatus.BAD_REQUEST);
	}
	
	@GetMapping(value = "/events/{product}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public List<AuditEvents> getTotalEvents(@PathVariable("product") String product) {
		log.info("Product find: {}", product);
		if(product != null) {
			String stringFind;
			if (product.equalsIgnoreCase("Todos")) {
				stringFind = "%%";
			} else {
				stringFind = new StringBuilder("%").append(product).append("%").toString();
			}
			List<AuditEvents> events = iAuditEvents.findEventsByProduct(stringFind);
			if (events != null && !events.isEmpty()) {
				return events;
			}
		}
		return new ArrayList<>();
	}
	
	@PostMapping(value = "/eventkeys", produces = { MediaType.APPLICATION_JSON_VALUE })
	public List<String> getEventKeys(@RequestBody EventsRequest eventsRequest) {
		if (eventsRequest != null && !eventsRequest.getKeys().isEmpty()) {
			List<String> events = iAuditEvents.findEventByKeys(eventsRequest.getKeys());
			if (events != null && !events.isEmpty()) {
				return events;
			}			
		}
		return new ArrayList<>();
	}	
}
