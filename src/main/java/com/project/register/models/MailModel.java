package com.project.register.models;

import java.util.List;
import java.util.Map;

import lombok.Data;

@Data
public class MailModel {
	private String from;
    private String to;
    private String subject;
    private String content;
    private List<Object> attachments;
    private Map<String, Object> props;
    
    public MailModel() {
    }

    public MailModel(String from, String to, String subject, String content) {
        this.from = from;
        this.to = to;
        this.subject = subject;
        this.content = content;
    }
}
