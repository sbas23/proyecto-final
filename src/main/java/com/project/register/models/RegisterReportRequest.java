package com.project.register.models;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
public class RegisterReportRequest {
	@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssZ", timezone = "America/Bogota")
	private Date dateFrom;
	@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssZ", timezone = "America/Bogota")
	private Date dateTo;
	private List<String> eventList;
	private List<String> modifiedUsernameList;
	private String description;
	private int page;
	private int offset;
}
