package com.project.register.models;

import java.util.Date;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

@Data
public class RegisterReportResponse {
	@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssZ", timezone = "America/Bogota")
	private Date eventDate;
	private String username;
	private String event;
	private Map<String, Object> metadata;
	private String id;
}
