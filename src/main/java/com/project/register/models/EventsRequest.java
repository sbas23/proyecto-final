package com.project.register.models;

import java.util.List;

import lombok.Data;

@Data
public class EventsRequest {
	private List<String> keys;
}
