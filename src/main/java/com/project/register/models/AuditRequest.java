package com.project.register.models;

import java.util.Date;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
public class AuditRequest { //
	@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssZ", timezone = "America/Bogota")
	private Date date;
	private String requestingProduct;
	private String requestingUser;
	private String event;
	private String modifiedUser;
	private int modifiedClientId;
	private Map<String, Object> description;
}
