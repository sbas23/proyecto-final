package com.project.register.rabbit;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMqConfiguration {
	private static final Logger logger = LoggerFactory.getLogger(RabbitMqConfiguration.class);
	
	@Value("${application.rabbit.addresses}")
	private String addresses;
	@Value("${application.rabbit.user}")
	private String rabbitUser;
	@Value("${application.rabbit.pass}")
	private String rabbitPass;
	
	@Value("${application.rabbit.consumerQueueNames}")
	private String queueNames;
	@Value("${application.rabbit.threads}")
	private int threads;
	
	@Bean
	MessageConsumer receiver() {
		return new MessageConsumer();
	}
	
	@Bean
	MessageListenerAdapter listenerAdapter(MessageConsumer messageConsumer) {
		return new MessageListenerAdapter(messageConsumer, jsonMessageConverter());
	}

	@Bean
	SimpleMessageListenerContainer container(MessageListenerAdapter listenerAdapter) {
		
		SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
		container.setConnectionFactory(connectionFactory());
		String[] queues = queueNames.split("\\|", -1);

		Map<String, Object> argumentsQueue = new HashMap<>();
		argumentsQueue.put("x-max-priority", 10);
		AmqpAdmin adm = new RabbitAdmin(connectionFactory());

		for (String queueName : queues) {
			Queue queue = new Queue(queueName, true, false, false, argumentsQueue);
			adm.declareQueue(queue);
			logger.info("Created queue: {}", queue.getName());
		}
	
		container.setQueueNames(queues);
		container.setConcurrentConsumers(threads);
		container.setPrefetchCount(1);
		logger.debug("Set queue Nnames, queue: {}",queueNames);
		container.setMessageListener(listenerAdapter);
		return container;
	}
	
	@Bean
	public ConnectionFactory connectionFactory() {
		CachingConnectionFactory connectionFactory = new CachingConnectionFactory();
		connectionFactory.setUsername(rabbitUser);
		connectionFactory.setPassword(rabbitPass);
		connectionFactory.setAddresses(addresses);
		logger.debug("Rabbit connection factory started, Adresses: {}, User: {}, Pass: {}",addresses,rabbitUser,rabbitPass);
		return connectionFactory;
	}

	@Bean
	public AmqpAdmin amqpAdmin() {
		return new RabbitAdmin(connectionFactory());
	}

	@Bean
	public MessageConverter jsonMessageConverter() {
		return new Jackson2JsonMessageConverter();
	}
}
