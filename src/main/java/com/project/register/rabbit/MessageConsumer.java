package com.project.register.rabbit;

import org.springframework.beans.factory.annotation.Autowired;

import com.project.register.models.AuditRequest;
import com.project.register.services.RegisterService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MessageConsumer {
	
	@Autowired
	RegisterService registerService;
	
	public void handleMessage(AuditRequest requestAudit) {
		log.info("Queue received : {}", requestAudit);
		registerService.responseRegisterRequest(requestAudit);
	}
	
}
