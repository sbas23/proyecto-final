package com.project.register.mysql.persistences;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.project.register.mysql.entities.AuditEvents;

public interface IAuditEvents extends CrudRepository<AuditEvents, Integer>{
	
	@Query(value= "SELECT id, event, searchWord, service FROM Register.AuditEvents WHERE product LIKE :product GROUP BY event ;", nativeQuery= true)
	List<AuditEvents> findEventsByProduct(@Param("product") String product);
	
	@Query(value= "SELECT searchWord FROM Register.AuditEvents WHERE event IN( :keys ) ;", nativeQuery= true)
	List<String> findEventByKeys(@Param("keys") List<String> keys);
	
	@Query(value= "SELECT * FROM Register.AuditEvents ;", nativeQuery= true)
	List<AuditEvents> findAll();
	
	Optional<AuditEvents> findFirstBySearchWord(String searchWord);
	
}
