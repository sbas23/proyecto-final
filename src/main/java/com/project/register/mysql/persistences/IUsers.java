package com.project.register.mysql.persistences;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.project.register.mysql.entities.Users;

public interface IUsers extends CrudRepository<Users, Integer>{
	Optional<Users> findByUsername(String username);
}
