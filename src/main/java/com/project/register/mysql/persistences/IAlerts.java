package com.project.register.mysql.persistences;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.project.register.mysql.entities.Alerts;

public interface IAlerts extends CrudRepository<Alerts, Integer>{
	
	@Query(value = "SELECT * FROM Register.alerts a WHERE a.userId = :userId AND a.auditEventId = :auditEventId ;", nativeQuery =  true)
	Alerts findByAuditEventIdAndUserId(@Param("userId") int userId, @Param("auditEventId") int auditEventId);
	
	@Modifying
	@Transactional
	@Query(value = "UPDATE Register.alerts SET eventCounter = eventCounter + 1 WHERE userId = :userId AND auditEventId = :auditEventId AND notificationSent = 0 ;", nativeQuery =  true)
	int updateEventCounterByAuditEventIdAndUserId(@Param("userId") int userId, @Param("auditEventId") int auditEventId);
	
	@Modifying
	@Transactional
	@Query(value = "UPDATE Register.alerts SET notificationSent = 1 WHERE id = :id ;", nativeQuery =  true)
	int updateNotificactionSentById(@Param("id") int id);

}
