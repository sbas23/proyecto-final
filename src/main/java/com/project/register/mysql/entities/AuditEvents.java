package com.project.register.mysql.entities;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "AuditEvents", schema = "Register")
public class AuditEvents {
	@Id
	private int id;
	private String event;
	private String searchWord;
	private String service;
}
