package com.project.register.mysql.entities;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
@Entity
@Table(name = "users", schema = "Register")
public class Alerts {
	@Id
	private int id;
	private int userId;
	private int auditEventId;
	private int eventCounter;
	private int trigger;
	private String severityLevel;
	private int notificationSent;
	private int status;
	@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssZ", timezone = "America/Bogota")
	private Date creationDate;
	@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssZ", timezone = "America/Bogota")
	private Date updateDate;
}
