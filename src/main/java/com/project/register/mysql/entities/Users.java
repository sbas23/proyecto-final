package com.project.register.mysql.entities;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
@Entity
@Table(name = "users", schema = "Register")
public class Users {
	@Id
	private int id;
	private String username;
	private String name;
	private String email;
	private String cellphone;
	@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssZ", timezone = "America/Bogota")
	private Date registrationDate;
}
