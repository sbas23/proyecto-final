package com.project.register.cache;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import com.project.register.mysql.entities.AuditEvents;
import com.project.register.mysql.persistences.IAuditEvents;

@Component
public class RegisterCache {
	
	@Autowired
	IAuditEvents iAuditEvents;
	
	@Cacheable("findAllEvents")
	public List<AuditEvents> findAll() {
		return iAuditEvents.findAll();
	}
	
	@CacheEvict(value = "findAllEvents",allEntries = true)
	public void evictAllCountryById() {
		// SONAR
	}

}
