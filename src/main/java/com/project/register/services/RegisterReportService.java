package com.project.register.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.project.register.cache.RegisterCache;
import com.project.register.models.RegisterReportRequest;
import com.project.register.models.RegisterReportResponse;
import com.project.register.mongo.entities.Events;
import com.project.register.mongo.persistences.IEvents;
import com.project.register.mysql.entities.AuditEvents;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class RegisterReportService {
	
	private static final String REPORT_KEY = "report";
	
	@Autowired
	private IEvents iEvents;
	@Autowired
	RegisterCache registerCache;
	
	public ResponseEntity<Object> getDetailByFilters(RegisterReportRequest auditReportRequest) {
		
		long total = iEvents.countDetailtReportByFilters(auditReportRequest);
		List<Events> detailReport= iEvents.getDetailtReportByFilters(auditReportRequest, true);		
		log.debug("detailReport {}", detailReport);

		ResponseEntity<Object> response = new ResponseEntity<>(HttpStatus.NO_CONTENT);
		if (detailReport != null && !detailReport.isEmpty()) {
			List<AuditEvents> events = registerCache.findAll();
			
			List<RegisterReportResponse> reportList = new ArrayList<>();
			for (Events event : detailReport) {
				RegisterReportResponse report = new RegisterReportResponse();
				report.setId(event.getId());
				report.setUsername(event.getModifiedUser());
				report.setEventDate(event.getDate());
				report.setEvent(getEventName(event.getEvent(), events));
				report.setMetadata(event.getDescription());
				reportList.add(report);
			}
			
			Map<String, Object> data = new HashMap<>();
			data.put(REPORT_KEY, reportList);
			data.put("total", total);
			response = ResponseEntity.ok().body(data);
		}
		return response;
	}
	
	private String getEventName (String key, List<AuditEvents> events) {
		for (AuditEvents auditEvents : events) {
			if (auditEvents.getSearchWord().equals(key)) {
				return auditEvents.getEvent();
			}
		}
		return key;
	}
	
}
