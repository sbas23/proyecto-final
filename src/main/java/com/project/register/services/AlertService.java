package com.project.register.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.project.register.cache.RegisterCache;
import com.project.register.models.AuditRequest;
import com.project.register.models.MailModel;
import com.project.register.mysql.entities.Alerts;
import com.project.register.mysql.entities.AuditEvents;
import com.project.register.mysql.entities.Users;
import com.project.register.mysql.persistences.IAlerts;
import com.project.register.mysql.persistences.IAuditEvents;
import com.project.register.mysql.persistences.IUsers;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AlertService {
	
	@Autowired
	IAlerts iAlerts;
	@Autowired
	IAuditEvents iauditEvent;
	@Autowired
	IUsers iUsers;
	@Autowired
	EmailService emailService;
	
	@Value("${spring.mail.username}")
	private String fromEmail;
	
	public void process(AuditRequest auditRequest) {
		Optional<AuditEvents> auditEvents = iauditEvent.findFirstBySearchWord(auditRequest.getEvent());
		log.debug("Audit Events: {}", auditEvents);
		Optional<Users> user = iUsers.findByUsername(auditRequest.getRequestingUser());
		log.debug("user: {}", user);
		
		if (auditEvents.isPresent() && user.isPresent()) {
			
			int wasUpdated  = iAlerts.updateEventCounterByAuditEventIdAndUserId(user.get().getId(), auditEvents.get().getId());
			if (wasUpdated == 1) {
				Alerts alert = iAlerts.findByAuditEventIdAndUserId(user.get().getId(), auditEvents.get().getId());
				log.info("Alert: {}", alert);
				if (alert.getEventCounter() >= alert.getTrigger()) {
					iAlerts.updateNotificactionSentById(alert.getId());
					
					String mailSubject =  new StringBuilder("Notificacion Alerta").toString();
					String body = new StringBuilder("Notificacion de alerta con Id: ").append(alert.getId()).toString();
					
					MailModel mailModel = new MailModel();
					mailModel.setTo(user.get().getEmail());
					mailModel.setSubject(mailSubject);
					mailModel.setContent(body);
					mailModel.setFrom(fromEmail);
					emailService.sendSimpleMessage(mailModel);
					log.info("Email Sent to: {}", user.get().getEmail());
				}
			}

		}
	}

}
