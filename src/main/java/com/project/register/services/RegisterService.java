package com.project.register.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.project.register.models.AuditRequest;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class RegisterService {

	@Autowired
	MongoOperations mongoOperations;
	@Autowired
	AlertService alertService;
	
	@Value("${application.mongo.collectionName}")
	String collectionName;
	
	public ResponseEntity<Object> responseRegisterRequest(AuditRequest auditRequest) {
		saveMongoAudit(auditRequest, collectionName);
		alertService.process(auditRequest);
		return ResponseEntity.ok().body(HttpStatus.OK.getReasonPhrase());
	}
	
	private void saveMongoAudit (AuditRequest auditRequest, String collectionName) {
		log.debug("Save mongo audit: {}, collectionName: {}", auditRequest, collectionName);
		mongoOperations.save(auditRequest, collectionName);
	}
}
