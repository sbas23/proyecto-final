import logging
import requests
import pika
import json
import random
import uuid

from datetime import datetime

# ---- Metodo de logueo ---- # /home/app/
logging.basicConfig(
      format='%(asctime)s %(levelname)s %(message)s',
      datefmt='%m/%d/%Y %I:%M:%S %p',
      filename='mock.log',
      level=logging.INFO)


def event_generator():
    # 1 (Error) 2 (Warning) 3 (info)
    level = [1,1,1,1,1,1,
        2,2,2,2,2,2,2,2,
        3,3,3,3,3,3,3,3,3,3,
        3,3,3,3,3,3,3,3,3,3,
        3,3,3,3,3,3,3,3,3,3,
        3,3,3,3,3,3,3,3,3,3,
        3,3,3,3,3,3,3,3,3,3,
        3,3,3,3,3,3,3,3,3,3,
        3,3,3,3,3,3,3,3,3,3,
        3,3,3,3,3,3,3,3,3,3,
        3,3,3,3,3,3
        ]
    
    error_events = ['queuing','operatorError','highTraffic','queuing','operatorError','highTraffic',
        'fullDisk','networkError','timeout',
        'poolBd','concurrence','inconsistencyInData','poolBd','concurrence','inconsistencyInData',
        'attacks',
        'configurationError','customerError','configurationError','customerError','configurationError',
        'customerError','configurationError','customerError','configurationError','customerError',
        'configurationError','customerError',
        'bug']
    
    warning_events = ['badRequest','badRequest','badRequest','badRequest','badRequest','badRequest',
        'loginFailed','loginFailed','loginFailed','loginFailed','loginFailed','loginFailed',
        'expiredPackage','expiredPackage','expiredPackage','expiredPackage','expiredPackage','expiredPackage','expiredPackage','expiredPackage',
        'balanceConsumed','balanceConsumed','balanceConsumed','balanceConsumed','balanceConsumed','balanceConsumed','balanceConsumed','balanceConsumed',
        'repeatedBase','repeatedBase','repeatedBase','repeatedBase','repeatedBase','repeatedBase','repeatedBase','repeatedBase',
        'reportGeneratedMultipleTimes','reportGeneratedMultipleTimes','reportGeneratedMultipleTimes','reportGeneratedMultipleTimes','reportGeneratedMultipleTimes']
    
    informational_events = ['sendQuickCampaign',
        'sendMasiveCampaign',
        'sendFlashCampaign',
        'sendTransactionalCampaign',
        'reportDownload',
        'sendCampaignAttached',
        'sendEmojisCampaign',
        'useAdministrator']
    
    users = ['usr001','usr002','usr003','usr004','usr005']
    users_admin = ['usr006','usr007','usr008','usr009','usr010']
    
    components = ['campaign', 'billing', 'report', 'delivery', 'kannel', 'sender', 'scmp', 'API', 'validator',
        'shortener', 'web', 'tools','rabbit' ]
    
    random.shuffle(level)
    select_level = random.choice(level)
    
    event = ''
    product = ''
    requestingUser = ''
    modifiedUser = ''
    description = {}
    
    if  select_level == 3:
        event = random.choice(informational_events)
        if event == 'reportDownload':
            product = 'Reports'
            description['reportId'] = str(uuid.uuid4())
            description['sizeMb'] = random.randint(0, 20)
            description['Format'] = 'csv'
            description['severity'] = 'info'
        elif event == 'useAdministrator':
            product = 'Admin'
            description['transactioId'] = str(uuid.uuid4())
            description['action'] = 'setting'
            description['severity'] = 'info'
        else:
            product = 'SMS'
            description['deliveryId'] = str(uuid.uuid4())
            description['totalAddressees'] = random.randint(0, 500000)
            description['content'] = 'Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto.'
            description['severity'] = 'info'
        
        requestingUser = random.choice(users_admin)
        modifiedUser = random.choice(users)   
            
    elif select_level == 2:
        event = random.choice(warning_events)
        product = 'SMS'
        requestingUser = random.choice(users_admin)
        modifiedUser = ''
        description['Id'] = str(uuid.uuid4())
        description['severity'] = 'warning'
        
    elif select_level == 1:
        event = random.choice(error_events)
        product = 'platform'
        requestingUser = 'system'
        modifiedUser = ''  
        description['Id'] = str(uuid.uuid4())
        description['component'] = random.choice(components)
        description['severity'] = 'error'
        
    
    start = datetime(2021, 1, 1)
    end =  datetime(2021, 10, 30)
    random_date = start + (end - start) * random.random()
    date = datetime.strftime(random_date,'%Y-%m-%dT00:00:00-0500Z')
    
    dic = {}
    dic['date'] = date
    dic['requestingProduct'] = product
    dic['requestingUser'] = requestingUser
    dic['event'] = event
    dic['modifiedUser'] = modifiedUser
    dic['modifiedClientId'] = '1'
    dic['description'] = description
    
    body = json.dumps(dic)
    #print(body)
    return body 


def request_rabbit_mq(limit):
    queueName = "service.register"
    typeId = "com.project.register.models.AuditRequest"
    
    # Establecer conexion
    #credentials = pika.PlainCredentials('guest', 'guest')
    #con = pika.BlockingConnection(pika.ConnectionParameters('localhost','5672','/',credentials))
    credentials = pika.PlainCredentials('******', '*******')
    con = pika.BlockingConnection(pika.ConnectionParameters('*******','5672','/',credentials))
    ch = con.channel()
    
    # Declarar la cola
    argument={'x-max-priority': 10}
    ch.queue_declare(queue=queueName, durable='true', arguments=argument)
    
    count = 0
    for i in range(0, limit):
        t = event_generator()
        print(t)
      
        # Publicar el mensaje
        header = {'__TypeId__':typeId}
        ch.basic_publish(exchange='', routing_key=queueName, body=t,
                 properties=pika.BasicProperties(content_type='application/json', headers=header))
        count += 1

    # Cerrar conexion
    con.close()
    return("Mensajes encolados: {0}".format(count))



def http_request(http_limit):
    url = 'http://**********:5042/register/trace'
    header = {'Content-type':'application/json'}
    for item in range(0, http_limit):
        body = event_generator()
        response = requests.post(url, headers = header, data = body)
    return response.text
    

def event_logic():
    http_limit = 10
    rabbit_limit = 18000
    
    logging.info('Inicio peticiones HTTP')
    http_request (http_limit)
    logging.info('Fin peticiones HTTP')
    
    logging.info('Inicio encolamient RabbitMQ')
    request_rabbit_mq(rabbit_limit)
    logging.info('Fin encolamient RabbitMQ')
    

def main():
    logging.info('Inicio proceso')
    event_logic()
    #event_generator()
    logging.info('Fin proceso')


if __name__== "__main__":
    main()
