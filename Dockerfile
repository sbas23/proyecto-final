FROM java:8

CMD ["mkdir", "/home/app/"]
COPY Files/app/__jarName__ /home/app/

CMD ["mkdir", "/config/"]
COPY Files/config/* /config/

EXPOSE __port__